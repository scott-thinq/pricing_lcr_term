<?php require_once('../Connections/carriers.php'); ?>
<?php

////////////////////  8 DIGIT //////////////////////
mysql_select_db($database_carriers, $carriers);
$query_get_8digit = "SELECT prefix FROM production_rates WHERE char_length(prefix) = 8 GROUP BY prefix ORDER BY prefix ASC";
$get_8digit = mysql_query($query_get_8digit, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_8digit);
$row_get_8digit = mysql_fetch_assoc($get_8digit);
$totalRows_get_8digit = mysql_num_rows($get_8digit);

// Drop Temporary Tables First
$sql = "DROP TEMPORARY TABLE IF EXISTS IP_lowest_inter_8DIGIT";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
$sql = "DROP TEMPORARY TABLE IF EXISTS IP_lowest_intra_8DIGIT";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold IP Lowest IntER 8digit results
$sql = " CREATE TEMPORARY TABLE `IP_lowest_inter_8DIGIT` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`inter_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MEMORY DEFAULT CHARSET=latin1 CHECKSUM=1 ";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold IP Lowest IntRA 8digit results
$sql = " CREATE TEMPORARY TABLE `IP_lowest_intra_8DIGIT` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`intra_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MEMORY DEFAULT CHARSET=latin1 CHECKSUM=1 ";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Loop through 8 digit prefixes and get the lowest rate / carrier combo for each.
do {
	$prefix = $row_get_8digit['prefix'];
	
	// Do IntER state Lookup
	mysql_select_db($database_carriers, $carriers);
	$query_get_lowest_inter = "SELECT carrier_id, inter_retail FROM production_rates WHERE prefix = '".$prefix."' AND carrier_id != 7 ORDER BY inter_retail ASC LIMIT 1";
	$get_lowest_inter = mysql_query($query_get_lowest_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_lowest_inter);
	$row_get_lowest_inter = mysql_fetch_assoc($get_lowest_inter);
	$totalRows_get_lowest_inter = mysql_num_rows($get_lowest_inter);
	
	// If we get a result insert the record
	if ($totalRows_get_lowest_inter > 0) {
		
		$carrier_id = $row_get_lowest_inter['carrier_id'];
		$inter_retail = $row_get_lowest_inter['inter_retail'];
		
		mysql_select_db($database_carriers, $carriers);
		$insert_inter = mysql_query("INSERT INTO IP_lowest_inter_8DIGIT (prefix, carrier_id, inter_retail) VALUES ('".$prefix."', '".$carrier_id."', '".$inter_retail."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_inter);
		
		unset ($carrier_id);
		unset ($inter_retail);
		
	}
	
	// Do IntRA state Lookup
	mysql_select_db($database_carriers, $carriers);
	$query_get_lowest_intra = "SELECT carrier_id, intra_retail FROM production_rates WHERE prefix = '".$prefix."' AND carrier_id != 7 ORDER BY intra_retail ASC LIMIT 1";
	$get_lowest_intra = mysql_query($query_get_lowest_intra, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_lowest_intra);
	$row_get_lowest_intra = mysql_fetch_assoc($get_lowest_intra);
	$totalRows_get_lowest_intra = mysql_num_rows($get_lowest_intra);
	
	// If we get a result insert the record
	if ($totalRows_get_lowest_intra > 0) {
		
		$carrier_id = $row_get_lowest_intra['carrier_id'];
		$intra_retail = $row_get_lowest_intra['intra_retail'];
		
		mysql_select_db($database_carriers, $carriers);
		$insert_intra = mysql_query("INSERT INTO IP_lowest_intra_8DIGIT (prefix, carrier_id, intra_retail) VALUES ('".$prefix."', '".$carrier_id."', '".$intra_retail."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_intra);
		
		unset ($carrier_id);
		unset ($intra_retail);
		
	}
	
} while ($row_get_8digit = mysql_fetch_assoc($get_8digit));

mysql_free_result($get_8digit);

mysql_free_result($get_lowest_inter);

mysql_free_result($get_lowest_intra);

////////////////////  7 DIGIT //////////////////////

mysql_select_db($database_carriers, $carriers);
$query_get_7digit = "SELECT prefix FROM production_rates WHERE char_length(prefix) = 7 GROUP BY prefix ORDER BY prefix ASC";
$get_7digit = mysql_query($query_get_7digit, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_7digit);
$row_get_7digit = mysql_fetch_assoc($get_7digit);
$totalRows_get_7digit = mysql_num_rows($get_7digit);

// Drop Temporary Tables First
$sql = "DROP TEMPORARY TABLE IF EXISTS IP_lowest_inter_7DIGIT";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
$sql = "DROP TEMPORARY TABLE IF EXISTS IP_lowest_intra_7DIGIT";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold IP Lowest IntER 7digit results
$sql = " CREATE TEMPORARY TABLE `IP_lowest_inter_7DIGIT` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`inter_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MEMORY DEFAULT CHARSET=latin1 CHECKSUM=1 ";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold IP Lowest IntRA 7digit results
$sql = " CREATE TEMPORARY TABLE `IP_lowest_intra_7DIGIT` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`intra_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MEMORY DEFAULT CHARSET=latin1 CHECKSUM=1 ";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Loop through 7 digit prefixes and get the lowest rate / carrier combo for each.
do {
	$prefix = $row_get_7digit['prefix'];
	
	// Do IntER state Lookup
	mysql_select_db($database_carriers, $carriers);
	$query_get_lowest_inter = "SELECT carrier_id, inter_retail FROM production_rates WHERE prefix = '".$prefix."' AND carrier_id != 7 ORDER BY inter_retail ASC LIMIT 1";
	$get_lowest_inter = mysql_query($query_get_lowest_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_lowest_inter);
	$row_get_lowest_inter = mysql_fetch_assoc($get_lowest_inter);
	$totalRows_get_lowest_inter = mysql_num_rows($get_lowest_inter);
	
	// If we get a result insert the record
	if ($totalRows_get_lowest_inter > 0) {
		
		$carrier_id = $row_get_lowest_inter['carrier_id'];
		$inter_retail = $row_get_lowest_inter['inter_retail'];
		
		mysql_select_db($database_carriers, $carriers);
		$insert_inter = mysql_query("INSERT INTO IP_lowest_inter_7DIGIT (prefix, carrier_id, inter_retail) VALUES ('".$prefix."', '".$carrier_id."', '".$inter_retail."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_inter);
		
		unset ($carrier_id);
		unset ($inter_retail);
		
	}
	
	// Do IntRA state Lookup
	mysql_select_db($database_carriers, $carriers);
	$query_get_lowest_intra = "SELECT carrier_id, intra_retail FROM production_rates WHERE prefix = '".$prefix."' AND carrier_id != 7 ORDER BY intra_retail ASC LIMIT 1";
	$get_lowest_intra = mysql_query($query_get_lowest_intra, $carriers)  or die ("Query failed: " . mysql_error() . " Actual query: " . $get_lowest_intra);
	$row_get_lowest_intra = mysql_fetch_assoc($get_lowest_intra);
	$totalRows_get_lowest_intra = mysql_num_rows($get_lowest_intra);
	
	// If we get a result insert the record
	if ($totalRows_get_lowest_intra > 0) {
		
		$carrier_id = $row_get_lowest_intra['carrier_id'];
		$intra_retail = $row_get_lowest_intra['intra_retail'];
		
		mysql_select_db($database_carriers, $carriers);
		$insert_intra = mysql_query("INSERT INTO IP_lowest_intra_7DIGIT (prefix, carrier_id, intra_retail) VALUES ('".$prefix."', '".$carrier_id."', '".$intra_retail."')", $carriers)  or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_intra);
		
		unset ($carrier_id);
		unset ($intra_retail);
		
	}
	
} while ($row_get_7digit = mysql_fetch_assoc($get_7digit));

mysql_free_result($get_7digit);

mysql_free_result($get_lowest_inter);

mysql_free_result($get_lowest_intra);


////////////////////  4 DIGIT //////////////////////

mysql_select_db($database_carriers, $carriers);
$query_get_4digit = "SELECT prefix FROM production_rates WHERE char_length(prefix) = 4 GROUP BY prefix ORDER BY prefix ASC";
$get_4digit = mysql_query($query_get_4digit, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_4digit);
$row_get_4digit = mysql_fetch_assoc($get_4digit);
$totalRows_get_4digit = mysql_num_rows($get_4digit);

// Drop Temporary Tables First
$sql = "DROP TEMPORARY TABLE IF EXISTS IP_lowest_inter_4DIGIT";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
$sql = "DROP TEMPORARY TABLE IF EXISTS IP_lowest_intra_4DIGIT";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold IP Lowest IntER 4digit results
$sql = " CREATE TEMPORARY TABLE `IP_lowest_inter_4DIGIT` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`inter_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MEMORY DEFAULT CHARSET=latin1 CHECKSUM=1 ";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold IP Lowest IntRA 4digit results
$sql = " CREATE TEMPORARY TABLE `IP_lowest_intra_4DIGIT` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`intra_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MEMORY DEFAULT CHARSET=latin1 CHECKSUM=1 ";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Loop through 4 digit prefixes and get the lowest rate / carrier combo for each.
do {
	$prefix = $row_get_4digit['prefix'];
	
	// Do IntER state Lookup
	mysql_select_db($database_carriers, $carriers);
	$query_get_lowest_inter = "SELECT carrier_id, inter_retail FROM production_rates WHERE prefix = '".$prefix."' AND carrier_id != 7 ORDER BY inter_retail ASC LIMIT 1";
	$get_lowest_inter = mysql_query($query_get_lowest_inter, $carriers)  or die ("Query failed: " . mysql_error() . " Actual query: " . $get_lowest_inter);
	$row_get_lowest_inter = mysql_fetch_assoc($get_lowest_inter);
	$totalRows_get_lowest_inter = mysql_num_rows($get_lowest_inter);
	
	// If we get a result insert the record
	if ($totalRows_get_lowest_inter > 0) {
		
		$carrier_id = $row_get_lowest_inter['carrier_id'];
		$inter_retail = $row_get_lowest_inter['inter_retail'];
		
		mysql_select_db($database_carriers, $carriers);
		$insert_inter = mysql_query("INSERT INTO IP_lowest_inter_4DIGIT (prefix, carrier_id, inter_retail) VALUES ('".$prefix."', '".$carrier_id."', '".$inter_retail."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_inter);
		
		unset ($carrier_id);
		unset ($inter_retail);
		
	}
	
	// Do IntRA state Lookup
	mysql_select_db($database_carriers, $carriers);
	$query_get_lowest_intra = "SELECT carrier_id, intra_retail FROM production_rates WHERE prefix = '".$prefix."' AND carrier_id != 7 ORDER BY intra_retail ASC LIMIT 1";
	$get_lowest_intra = mysql_query($query_get_lowest_intra, $carriers)  or die ("Query failed: " . mysql_error() . " Actual query: " . $get_lowest_intra);
	$row_get_lowest_intra = mysql_fetch_assoc($get_lowest_intra);
	$totalRows_get_lowest_intra = mysql_num_rows($get_lowest_intra);
	
	// If we get a result insert the record
	if ($totalRows_get_lowest_intra > 0) {
		
		$carrier_id = $row_get_lowest_intra['carrier_id'];
		$intra_retail = $row_get_lowest_intra['intra_retail'];
		
		mysql_select_db($database_carriers, $carriers);
		$insert_intra = mysql_query("INSERT INTO IP_lowest_intra_4DIGIT (prefix, carrier_id, intra_retail) VALUES ('".$prefix."', '".$carrier_id."', '".$intra_retail."')", $carriers)  or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_intra);
		
		unset ($carrier_id);
		unset ($intra_retail);
		
	}
	
} while ($row_get_4digit = mysql_fetch_assoc($get_4digit));

mysql_free_result($get_4digit);

mysql_free_result($get_lowest_inter);

mysql_free_result($get_lowest_intra);

////////////////////  7 AND 8 DIGIT MASHUP //////////////////////

//// intER state Lookups ////

// Get 8 digit lowest inter
mysql_select_db($database_carriers, $carriers);
$query_get_8digit_inter = "SELECT prefix, left(prefix,7) as npanxx, carrier_id, inter_retail FROM IP_lowest_inter_8DIGIT GROUP BY prefix ORDER BY prefix ASC";
$get_8digit_inter = mysql_query($query_get_8digit_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 1: " . $get_8digit_inter);
$row_get_8digit_inter = mysql_fetch_assoc($get_8digit_inter);
$totalRows_get_8digit_inter = mysql_num_rows($get_8digit_inter);

do {
	
	$digit8_prefix = $row_get_8digit_inter['prefix'];
	$digit8_npanxx = $row_get_8digit_inter['npanxx'];
	$digit8_carrier_id = $row_get_8digit_inter['carrier_id'];
	$digit8_inter_retail = $row_get_8digit_inter['inter_retail'];
	
	// Lookup 7 digit rate
	mysql_select_db($database_carriers, $carriers);
	$query_get_7digit_inter = "SELECT prefix, carrier_id, inter_retail FROM IP_lowest_inter_7DIGIT WHERE prefix = '".$digit8_npanxx."'";
	$get_7digit_inter = mysql_query($query_get_7digit_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 2: " . $get_7digit_inter);
	$row_get_7digit_inter = mysql_fetch_assoc($get_7digit_inter);
	$totalRows_get_7digit_inter = mysql_num_rows($get_7digit_inter);
	
	// If we get a 7 digit match check it out
	if ($totalRows_get_7digit_inter > 0) {
	
		$digit7_prefix = $row_get_7digit_inter['prefix'];
		$digit7_carrier_id = $row_get_7digit_inter['carrier_id'];
		$digit7_inter_retail = $row_get_7digit_inter['inter_retail'];
	
		// If the 8 digit price is greater than or equal to the 7 digit price, remove the 8 digit prefix
		if ($digit8_inter_retail >= $digit7_inter_retail) {
		
			mysql_select_db($database_carriers, $carriers);
			$delete_inter = mysql_query("DELETE FROM IP_lowest_inter_8DIGIT WHERE prefix = '".$digit8_prefix."'", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 3: " . $delete_inter);		
			//echo $digit8_prefix;
			//echo "----";
			//echo $digit8_carrier_id;
			//echo "----";
			//echo $digit8_inter_retail;
			//echo "\n";
		
		} 
		
	}

	
} while ($row_get_8digit_inter = mysql_fetch_assoc($get_8digit_inter));



mysql_free_result($get_8digit_inter);

mysql_free_result($get_7digit_inter);


//// intRA state Lookups ////

// Get 8 digit lowest intra
mysql_select_db($database_carriers, $carriers);
$query_get_8digit_intra = "SELECT prefix, left(prefix,7) as npanxx, carrier_id, intra_retail FROM IP_lowest_intra_8DIGIT GROUP BY prefix ORDER BY prefix ASC";
$get_8digit_intra = mysql_query($query_get_8digit_intra, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 4: " . $get_8digit_intra);
$row_get_8digit_intra = mysql_fetch_assoc($get_8digit_intra);
$totalRows_get_8digit_intra = mysql_num_rows($get_8digit_intra);

do {
	
	$digit8_prefix = $row_get_8digit_intra['prefix'];
	$digit8_npanxx = $row_get_8digit_intra['npanxx'];
	$digit8_carrier_id = $row_get_8digit_intra['carrier_id'];
	$digit8_intra_retail = $row_get_8digit_intra['intra_retail'];
	
	// Lookup 7 digit rate
	mysql_select_db($database_carriers, $carriers);
	$query_get_7digit_intra = "SELECT prefix, carrier_id, intra_retail FROM IP_lowest_intra_7DIGIT WHERE prefix = '".$digit8_npanxx."'";
	$get_7digit_intra = mysql_query($query_get_7digit_intra, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 5: " . $get_7digit_intra);
	$row_get_7digit_intra = mysql_fetch_assoc($get_7digit_intra);
	$totalRows_get_7digit_intra = mysql_num_rows($get_7digit_intra);
	
// If we get a 7 digit match check it out
	if ($totalRows_get_7digit_intra > 0) {
	
		$digit7_prefix = $row_get_7digit_intra['prefix'];
		$digit7_carrier_id = $row_get_7digit_intra['carrier_id'];
		$digit7_intra_retail = $row_get_7digit_intra['intra_retail'];
	
		// If the 8 digit price is greater than or equal to the 7 digit price, remove the 8 digit prefix
		if ($digit8_intra_retail >= $digit7_intra_retail) {
		
			mysql_select_db($database_carriers, $carriers);
			$delete_intra = mysql_query("DELETE FROM IP_lowest_intra_8DIGIT WHERE prefix = '".$digit8_prefix."'", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 6: " . $delete_intra);
		
			//echo $digit8_prefix;
			//echo "----";
			//echo $digit8_carrier_id;
			//echo "----";
			//echo $digit8_intra_retail;
			//echo "\n";
		
		}
		
	}

	
} while ($row_get_8digit_intra = mysql_fetch_assoc($get_8digit_intra));


mysql_free_result($get_8digit_intra);

mysql_free_result($get_7digit_intra);


////////////////////  4 AND 7 DIGIT MASHUP //////////////////////


//// intER state Lookups ////

// Get 7 digit lowest inter
mysql_select_db($database_carriers, $carriers);
$query_get_7digit_inter = "SELECT prefix, left(prefix,4) as npa, carrier_id, inter_retail FROM IP_lowest_inter_7DIGIT GROUP BY prefix ORDER BY prefix ASC";
$get_7digit_inter = mysql_query($query_get_7digit_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 7: " . $get_7digit_inter);
$row_get_7digit_inter = mysql_fetch_assoc($get_7digit_inter);
$totalRows_get_7digit_inter = mysql_num_rows($get_7digit_inter);

do {
	
	$digit7_prefix = $row_get_7digit_inter['prefix'];
	$digit7_npa = $row_get_7digit_inter['npa'];
	$digit7_carrier_id = $row_get_7digit_inter['carrier_id'];
	$digit7_inter_retail = $row_get_7digit_inter['inter_retail'];
	
	// Lookup 4 digit rate
	mysql_select_db($database_carriers, $carriers);
	$query_get_4digit_inter = "SELECT prefix, carrier_id, inter_retail FROM IP_lowest_inter_4DIGIT WHERE prefix = '".$digit7_npa."'";
	$get_4digit_inter = mysql_query($query_get_4digit_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 8: " . $get_4digit_inter);
	$row_get_4digit_inter = mysql_fetch_assoc($get_4digit_inter);
	$totalRows_get_4digit_inter = mysql_num_rows($get_4digit_inter);
	
	// If we get a 4 digit match check it out
	if ($totalRows_get_4digit_inter > 0) {
	
		$digit4_prefix = $row_get_4digit_inter['prefix'];
		$digit4_carrier_id = $row_get_4digit_inter['carrier_id'];
		$digit4_inter_retail = $row_get_4digit_inter['inter_retail'];
	
		// If the 7 digit price is greater than or equal to the 4 digit price, remove the 7 digit prefix
		if ($digit7_inter_retail >= $digit4_inter_retail) {
		
			mysql_select_db($database_carriers, $carriers);
			$delete_inter = mysql_query("DELETE FROM IP_lowest_inter_7DIGIT WHERE prefix = '".$digit7_prefix."'", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 9: " . $delete_inter);
		
			//echo $digit7_prefix;
			//echo "----";
			//echo $digit7_carrier_id;
			//echo "----";
			//echo $digit7_inter_retail;
			//echo "\n";
		
		} else {
		
			mysql_select_db($database_carriers, $carriers);
			$delete_inter = mysql_query("DELETE FROM IP_lowest_inter_4DIGIT WHERE prefix = '".$digit4_prefix."'", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 10: " . $delete_inter);
		
			//echo $digit4_prefix;
			//echo "----";
			//echo $digit4_carrier_id;
			//echo "----";
			//echo $digit4_inter_retail;
			//echo "\n";
		
		}
		
	}

	
} while ($row_get_7digit_inter = mysql_fetch_assoc($get_7digit_inter));



mysql_free_result($get_7digit_inter);

mysql_free_result($get_4digit_inter);


//// intRA state Lookups ////

// Get 7 digit lowest intra
mysql_select_db($database_carriers, $carriers);
$query_get_7digit_intra = "SELECT prefix, left(prefix,4) as npa, carrier_id, intra_retail FROM IP_lowest_intra_7DIGIT GROUP BY prefix ORDER BY prefix ASC";
$get_7digit_intra = mysql_query($query_get_7digit_intra, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 11: " . $get_7digit_intra);
$row_get_7digit_intra = mysql_fetch_assoc($get_7digit_intra);
$totalRows_get_7digit_intra = mysql_num_rows($get_7digit_intra);

do {
	
	$digit7_prefix = $row_get_7digit_intra['prefix'];
	$digit7_npa = $row_get_7digit_intra['npa'];
	$digit7_carrier_id = $row_get_7digit_intra['carrier_id'];
	$digit7_intra_retail = $row_get_7digit_intra['intra_retail'];
	
	// Lookup 4 digit rate
	mysql_select_db($database_carriers, $carriers);
	$query_get_4digit_intra = "SELECT prefix, carrier_id, intra_retail FROM IP_lowest_intra_4DIGIT WHERE prefix = '".$digit7_npa."'";
	$get_4digit_intra = mysql_query($query_get_4digit_intra, $carriers) or die ("Query failed: " . mysql_error() . " Actual query 12: " . $get_4digit_intra);
	$row_get_4digit_intra = mysql_fetch_assoc($get_4digit_intra);
	$totalRows_get_4digit_intra = mysql_num_rows($get_4digit_intra);
	
// If we get a 4 digit match check it out
	if ($totalRows_get_4digit_intra > 0) {
	
		$digit4_prefix = $row_get_4digit_intra['prefix'];
		$digit4_carrier_id = $row_get_4digit_intra['carrier_id'];
		$digit4_intra_retail = $row_get_4digit_intra['intra_retail'];
	
		// If the 7 digit price is greater than or equal to the 4 digit price, remove the 7 digit prefix
		if ($digit7_intra_retail >= $digit4_intra_retail) {
		
			mysql_select_db($database_carriers, $carriers);
			$delete_intra = mysql_query("DELETE FROM IP_lowest_intra_7DIGIT WHERE prefix = '".$digit7_prefix."'", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 13: " . $delete_intra);
		
			//echo $digit7_prefix;
			//echo "----";
			//echo $digit7_carrier_id;
			//echo "----";
			//echo $digit7_intra_retail;
			//echo "\n";
		
		} else {
		
			mysql_select_db($database_carriers, $carriers);
			$delete_intra = mysql_query("DELETE FROM IP_lowest_intra_4DIGIT WHERE prefix = '".$digit4_prefix."'", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 14: " . $delete_intra);
		
			//echo $digit4_prefix;
			//echo "----";
			//echo $digit4_carrier_id;
			//echo "----";
			//echo $digit4_intra_retail;
			//echo "\n";
		
		}
		
	}

	
} while ($row_get_7digit_intra = mysql_fetch_assoc($get_7digit_intra));


mysql_free_result($get_7digit_intra);

mysql_free_result($get_4digit_intra);

////////////////////  STORE FINAL RESULTS //////////////////////

// CREATE NEW PERMANENT TABLES FOR STORING FINAL MASHUP //

// Drop Tables First
$sql = "DROP TABLE IF EXISTS IP_lowest_inter_NEW";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query 15: " . $sql);
$sql = "DROP TABLE IF EXISTS IP_lowest_intra_NEW";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query 16: " . $sql);

// Create table to hold IP Lowest IntER results
$sql = " CREATE TABLE `IP_lowest_inter_NEW` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`inter_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1 ";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query 17: " . $sql);

// Create table to hold IP Lowest IntRA results
$sql = " CREATE TABLE `IP_lowest_intra_NEW` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
  		`prefix` bigint(20) NOT NULL,
  		`carrier_id` int(10) NOT NULL,
  		`intra_retail` decimal(10,6) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`),
  		KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  		KEY `idx_prefix` (`prefix`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1 ";
 mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query 18: " . $sql);

// Insert final values into permanent tables
mysql_select_db($database_carriers, $carriers);
$insert_inter = mysql_query("INSERT INTO IP_lowest_inter_NEW (prefix, carrier_id, inter_retail) SELECT prefix, carrier_id, inter_retail FROM IP_lowest_inter_8DIGIT
UNION ALL
SELECT prefix, carrier_id, inter_retail FROM IP_lowest_inter_7DIGIT 
UNION ALL
SELECT prefix, carrier_id, inter_retail FROM IP_lowest_inter_4DIGIT 
ORDER BY prefix ASC", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 19: " . $insert_inter);

mysql_select_db($database_carriers, $carriers);
$insert_intra = mysql_query("INSERT INTO IP_lowest_intra_NEW (prefix, carrier_id, intra_retail) SELECT prefix, carrier_id, intra_retail FROM IP_lowest_intra_8DIGIT
UNION ALL
SELECT prefix, carrier_id, intra_retail FROM IP_lowest_intra_7DIGIT 
UNION ALL
SELECT prefix, carrier_id, intra_retail FROM IP_lowest_intra_4DIGIT 
ORDER BY prefix ASC", $carriers) or die ("Query failed: " . mysql_error() . " Actual query 20: " . $insert_intra);
?>