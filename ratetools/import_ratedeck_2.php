<?php require_once('../Connections/carriers.php'); ?>
<?php require_once('../Connections/prod_opensips.php'); ?>
<?php
$STDERR = fopen('/var/log/rate_import.log', 'a+');
//check for file upload
if(isset($_FILES['ratedeck']) && is_uploaded_file($_FILES['ratedeck']['tmp_name'])){

	//upload directory
	$upload_dir = "/var/www/cdr_analysis/ratetools/csv_dir/";

	//create file name
	$file_path = $upload_dir . $_FILES['ratedeck']['name'];

	//move uploaded file to upload dir
	if (!move_uploaded_file($_FILES['ratedeck']['tmp_name'], $file_path)) {

		//error moving upload file
		fwrite($STDERR, date("Y-m-d H:i:s") . " " . "Error moving file upload \n");

	}
	//open the csv file for reading
	ini_set('auto_detect_line_endings', true);
	$handle = fopen($file_path, 'r');

	//turn off autocommit and delete the product table
	//mysql_query("SET AUTOCOMMIT=0");
	//mysql_query("BEGIN");
	//mysql_query("TRUNCATE TABLE product_table") or die(mysql_error());
    
	// Set ratedeck variables
	$carrier_id = $_POST['carrier_id'];
	$type = $_POST['type'];
	$effective = $_POST['effective'];
	// Insert ratedeck info and get ratedeck ID
	mysql_select_db($database_carriers, $carriers);
	$insert_ratedeck = mysql_query("INSERT INTO ratedecks (type, carrier_id, effective) VALUES ('".$type."', '".$carrier_id."', '".$effective."')", $carriers) or die(mysql_error());
	
	$query_get_ratedeckid = "SELECT id FROM ratedecks ORDER BY id DESC limit 1";
	$get_ratedeckid = mysql_query($query_get_ratedeckid, $carriers) or die(mysql_error());
	$row_get_ratedeckid = mysql_fetch_assoc($get_ratedeckid);
	$totalRows_get_ratedeckid = mysql_num_rows($get_ratedeckid);
	
	$truncate_exceptions = mysql_query("TRUNCATE pricing_exceptions", $carriers) or die(mysql_error());
		
	//$rules = mysql_fetch_array($get_rules);
	
	// Set ratedeck ID
	$ratedeck_id = $row_get_ratedeckid['id'];
	while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
		
		// If this is a domestic Deck do this.
		if ($type == 1 || $type == 2) {
			
			//Access field data in $data array ex.
			$prefix = trim(mysql_real_escape_string($data[0]));
			$inter_cost = trim(mysql_real_escape_string($data[1]));
			$intra_cost = trim(mysql_real_escape_string($data[2]));
			$initial = 6;
			$additional = 6;
		
			// Normalize prifix to start with "1"
			
			if (substr($prefix, 0, 1) != "1") {
				$prefix = 1 . $prefix;
			}
		

			// Check for TF
			$npa = substr($prefix, 0, 4);

			$tf = array("1800", "1833", "1844", "1855", "1866", "1877", "1888");
			if (in_array($npa, $tf)) {
				fwrite($STDERR, date("Y-m-d H:i:s") . " " . "Prefix: $prefix is Toll Free, skipping\n");
			} else {

			//fwrite($STDERR, date("Y-m-d H:i:s") . " " . "Working on Prefix: $prefix Carrier ID: $carrier_id InterCost: $inter_cost IntraCost: $intra_cost Initial: $initial Additional: $additional\n");
			
			// First Check to see if there are any Overrides
			$query_get_overrides = "SELECT inter_retail, intra_retail FROM prefix_overrides WHERE prefix = $prefix AND carrier_id = $carrier_id";
			$get_overrides = mysql_query($query_get_overrides, $carriers) or die(mysql_error());
			$row_get_overrides = mysql_fetch_assoc($get_overrides);
			$totalRows_get_overrides = mysql_num_rows($get_overrides);

			// If we get a result then use the override price, skip all else

			// Next Check to see if there are any Margin Overrides
			mysql_select_db($database_carriers, $carriers);
			$query_get_margin_overrides = "SELECT margin FROM prefix_margin_override WHERE prefix = $prefix";
			$get_margin_overrides = mysql_query($query_get_margin_overrides, $carriers) or die(mysql_error());
			$row_get_margin_overrides = mysql_fetch_assoc($get_margin_overrides);
			$totalRows_get_margin_overrides = mysql_num_rows($get_margin_overrides);

			// Next Check to see if there are any FLOOR Overrides
			mysql_select_db($database_carriers, $carriers);
			$query_get_floor = "SELECT inter_retail, intra_retail FROM prefix_floor WHERE prefix = $prefix";
			$get_floor = mysql_query($query_get_floor, $carriers) or die(mysql_error());
			$row_get_floor = mysql_fetch_assoc($get_floor);
			$totalRows_get_floor = mysql_num_rows($get_floor);

			// Finally Get margin rules
			mysql_select_db($database_carriers, $carriers);
			$query_get_rules = "SELECT rule, rate, margin FROM margin_rules ORDER BY rate DESC";
			$get_rules = mysql_query($query_get_rules, $carriers) or die(mysql_error());
			$row_get_rules = mysql_fetch_assoc($get_rules);
			$totalRows_get_rules = mysql_num_rows($get_rules);


			// If We get an override then calculate the retail rates using the override skip all else
			if ($totalRows_get_overrides > 0) {

				$source = "Override";
				$inter_retail = $row_get_overrides['inter_retail'];
				$intra_retail = $row_get_overrides['intra_retail'];

				fwrite($STDERR, date("Y-m-d H:i:s") . " " . "CarrierID: $carrier_id Prefix: $prefix has a manual Override of InterRetail: $inter_retail - IntraRetail: $intra_retail\n");

			}
			// Else if we get a margin override, calcualte retail rates using the margin override skip all else
			else if ($totalRows_get_margin_overrides > 0) {

				$source = "Override";
				$margin = $row_get_margin_overrides['margin'];
				$inter_retail = round(($inter_cost / (1 - $margin)),6);
				$intra_retail = round(($intra_cost / (1 - $margin)),6);

				fwrite($STDERR, date("Y-m-d H:i:s") . " " . "Prefix: $prefix has a margin Override of $margin\n");

			}			
			// Else use the regular rules
			else {

				$source = "Margin Rule";
				//fwrite($STDERR, date("Y-m-d H:i:s") . " " . "Using Margin Rules to price Prefix: $prefix\n");
			
				// Loop through rules and set retail rates
				do {
					$rate = $row_get_rules['rate'];
					$margin = $row_get_rules['margin'];
				
					if($inter_cost <= $rate) {
						$inter_retail = round(($inter_cost / (1 - $margin)),6);
				
					}
				
					if($intra_cost <= $rate) {
						$intra_retail = round(($intra_cost / (1 - $margin)),6);
				
					}
				
				} while ($row_get_rules = mysql_fetch_assoc($get_rules));

				//fwrite($STDERR, date("Y-m-d H:i:s") . " " . "Prefix: $prefix - InterRetail: $inter_retail - IntraRetail: $intra_retail\n");

				// Now that we've priced using the rules lets check if there are any minimum overrides
				if ($totalRows_get_floor > 0) {
					$inter_min = $row_get_floor['inter_retail'];
					$intra_min = $row_get_floor['intra_retail'];

					// Compare minimum to price
					if ($inter_retail < $inter_min) {
						$source = "Floor";

						fwrite($STDERR, date("Y-m-d H:i:s") . " " . "CarrierID: $carrier_id Prefix: $prefix - InterRetail: $inter_retail is less than Floor: $inter_min. Using Floor: $inter_min\n");
						$inter_retail = $inter_min;
											}
					if ($intra_retail < $intra_min) {
						$source = "Floor";

						fwrite($STDERR, date("Y-m-d H:i:s") . " " . "CarrierID: $carrier_id Prefix: $prefix - IntraRetail: $intra_retail is less than Floor: $intra_min. Using Floor: $intra_min\n");
						$intra_retail = $intra_min;
					}
				}
			}
			
			// Apply minimum margin
			$min_margin = 0.45;
			$inter_min_sell = round(($inter_cost / (1 - $min_margin)),6);
			$intra_min_sell = round(($intra_cost / (1 - $min_margin)),6);

			// PREVENTS US LOSING MONEY!! -- If the retail rate is less than the minimum margin, override the retail
			if ($inter_retail < $inter_min_sell) {
				$jur = "Inter";
				fwrite($STDERR, date("Y-m-d H:i:s") . " " . "CarrierID: $carrier_id Prefix: $prefix - InterRetail: $inter_retail is less than MinSell: $inter_min_sell. Using Min: $inter_min_sell\n");

				// Set new Inter Retail
				$inter_retail = $inter_min_sell;
			}
			if ($intra_retail < $intra_min_sell) {
				$jur = "Intra";
				fwrite($STDERR, date("Y-m-d H:i:s") . " " . "CarrierID: $carrier_id Prefix: $prefix - IntraRetail: $intra_retail is less than MinSell: $intra_min_sell. Using Min: $intra_min_sell\n");

				// Set new Intra Retail
				$intra_retail = $intra_min_sell;
			}
			
			//Use data to insert into analytics db
			mysql_select_db($database_carriers, $carriers);
			$insert_ratedeck = mysql_query("INSERT INTO rates (carrier_id, ratedeck_id, prefix, inter_cost, intra_cost, inter_min_sell, intra_min_sell, inter_retail, intra_retail, initial, additional) VALUES ('".$carrier_id."', '".$ratedeck_id."', '".$prefix."', '".$inter_cost."', '".$intra_cost."', '".$inter_min_sell."', '".$intra_min_sell."', '".$inter_retail."', '".$intra_retail."', '".$initial."', '".$additional."')", $carriers) or die(mysql_error());
			
			//if (isset($source)) {
				
				// Add to exception Report
			//	mysql_select_db($database_carriers, $carriers);
			//	$inter_exception = mysql_query("INSERT INTO pricing_exceptions (source, carrier_id, jurisdiction, prefix, cost, min_sell, retail) VALUES ('".$source."', '".$carrier_id."', '".$jur."', '".$prefix."', '".$inter_cost."', '".$inter_min_sell."', '".$inter_retail."')", $carriers) or die(mysql_error());	
				
			//}
		
		} // end of Toll Free ELSE
		}
		
		// If this is an INTL deck do this.
		else if ($type == 3) {

			// Identify one off country codes
			$spec_codes = array(502,503,504);

			// Set Prefix and Rate Variables
			$description = strtoupper(trim(mysql_real_escape_string($data[0])));
			$prefix = trim(mysql_real_escape_string($data[1]));
			$cost = trim(mysql_real_escape_string($data[2]));
			$min_margin = 0.30;
			$margin = 0.40;
			
			$first_char = substr($prefix, 0, 1);
			$two_chars = substr($prefix, 0, 2);
			$three_chars = substr($prefix, 0, 3);
			
			// If the CC is Mexico set billing to 60/60, else set to 1/1
			if ($two_chars == 52) {
				
				$initial = 60;
				$additional = 60;
			
			} else {
				
				$initial = 1;
				$additional = 1;
			}
			
			// special code pricing
			if (in_array($three_chars, $spec_codes)) {
				
				// Set Minimum Sell Price and Retail Rate
				$min_sell = round(($cost / (1 - $min_margin)),6);
				$retail = round(($cost / (1 - 0.37)),6);
				
			} else {
			
				// Set Minimum Sell Price and Retail Rate
				$min_sell = round(($cost / (1 - $min_margin)),6);
				$retail = round(($cost / (1 - $margin)),6);
			}
			
			// If the prefix isn't Domestic, insert it into the DB
			if ($first_char != 1) {
			
				// Insert Intl Analytics DB
				mysql_select_db($database_carriers, $carriers);
				$insert_ratedeck = mysql_query("INSERT INTO intl_rates (carrier_id, ratedeck_id, prefix, cost, min_sell, retail, initial, additional, description, effective_date) VALUES ('".$carrier_id."', '".$ratedeck_id."', '".$prefix."', '".$cost."', '".$min_sell."', '".$retail."', '".$initial."', '".$additional."', '".$description."', '".$effective."')", $carriers) or die(mysql_error());
				
				// Insert Costs into Production DB
				mysql_select_db($database_prod_opensips, $prod_opensips);
				$insert_intl_prod = mysql_query("INSERT INTO costs_international (carrier_id, prefix, min_sell, cost, initial, additional, effective_date) VALUES ('".$carrier_id."', '".$prefix."', '".$min_sell."', '".$cost."', '".$initial."', '".$additional."', '".$effective."')", $prod_opensips) or die(mysql_error());
			
			}
		
		} // End Intl Deck
		
	} // End File loop

	//commit the data to the database
	//mysql_query("COMMIT");
	//mysql_query("SET AUTOCOMMIT=1");

	//delete csv file
	unlink($file_path);
	fclose($STDERR);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META HTTP-EQUIV="refresh" CONTENT="3; URL=index.php">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Import Rate Deck</title>
</head>

<body>

<center>
          <p>&nbsp;</p>
          <p>Importing Rate Deck
            <br />
            <br />
          <img src="images/icons/ajax-loader-1.gif" /></p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
</center>
</body>
</html>
