<?php require_once('../Connections/carriers.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "update_carrier")) {
  $updateSQL = sprintf("UPDATE carriers SET type=%s, name=%s, active=%s, analysis=%s WHERE id=%s",
                       GetSQLValueString($_POST['type'], "int"),
                       GetSQLValueString($_POST['name'], "text"),
                       GetSQLValueString($_POST['status'], "int"),
                       GetSQLValueString(isset($_POST['analysis']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_carriers, $carriers);
  $Result1 = mysql_query($updateSQL, $carriers) or die(mysql_error());

  $updateGoTo = "index.php?list_carriers=1";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$varID_get_carrier = "-1";
if (isset($_GET['carrier_id'])) {
  $varID_get_carrier = $_GET['carrier_id'];
}
// Get Carriers
mysql_select_db($database_carriers, $carriers);
$query_get_carrier = sprintf("select * from carriers where id = %s", GetSQLValueString($varID_get_carrier, "int"));
$get_carrier = mysql_query($query_get_carrier, $carriers) or die(mysql_error());
$row_get_carrier = mysql_fetch_assoc($get_carrier);
$totalRows_get_carrier = mysql_num_rows($get_carrier);

// Get Ratedeck Types
mysql_select_db($database_carriers, $carriers);
$query_get_decktypes = "SELECT * FROM ratedeck_types ORDER BY name ASC";
$get_decktypes = mysql_query($query_get_decktypes, $carriers) or die(mysql_error());
$row_get_decktypes = mysql_fetch_assoc($get_decktypes);
$totalRows_get_decktypes = mysql_num_rows($get_decktypes);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="643" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="611" height="142" align="center" valign="top"><table width="500" border="1">
      <tr>
        <td><a href="index.php?list_carriers=1">Back</a></td>
      </tr>
      <tr>
        <td class="center_bold">Edit Carrier Information</td>
      </tr>
    </table>
      <form id="update_carrier" name="update_carrier" method="POST" action="<?php echo $editFormAction; ?>">
        <table width="500" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td width="173">Name
            <input name="id" type="hidden" id="id" value="<?php echo $row_get_carrier['id']; ?>" /></td>
            <td width="321"><label for="name"></label>
            <input name="name" type="text" id="name" value="<?php echo $row_get_carrier['name']; ?>" /></td>
          </tr>
          <tr>
            <td>Traffic Type:</td>
            <td><label for="type"></label>
              <select name="type" id="type">
                <option value="" <?php if (!(strcmp("", $row_get_carrier['type']))) {echo "selected=\"selected\"";} ?>>Select Type</option>
                <?php
do {  
?>
                <option value="<?php echo $row_get_decktypes['id']?>"<?php if (!(strcmp($row_get_decktypes['id'], $row_get_carrier['type']))) {echo "selected=\"selected\"";} ?>><?php echo $row_get_decktypes['name']?></option>
                <?php
} while ($row_get_decktypes = mysql_fetch_assoc($get_decktypes));
  $rows = mysql_num_rows($get_decktypes);
  if($rows > 0) {
      mysql_data_seek($get_decktypes, 0);
	  $row_get_decktypes = mysql_fetch_assoc($get_decktypes);
  }
?>
            </select></td>
          </tr>
          <tr>
            <td>Status</td>
            <td><input <?php if (!(strcmp($row_get_carrier['active'],"1"))) {echo "checked=\"checked\"";} ?> type="radio" name="status" id="radio" value="1" />
              Active 
            <input <?php if (!(strcmp($row_get_carrier['active'],"0"))) {echo "checked=\"checked\"";} ?> type="radio" name="status" id="radio2" value="0" />
            Inactive
            <label for="status"></label></td>
          </tr>
          <tr>
            <td>Include in Analysis?</td>
            <td><input <?php if (!(strcmp($row_get_carrier['analysis'],1))) {echo "checked=\"checked\"";} ?> name="analysis" type="checkbox" id="analysis" value="1" />
            <label for="analysis"></label></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="button" id="button" value="Update" /></td>
          </tr>
        </table>
        <input type="hidden" name="MM_update" value="update_carrier" />
      </form>
    <p>&nbsp;</p></td>
  </tr>
</table>
</body>
</html>
<?php
mysql_free_result($get_carrier);

mysql_free_result($get_decktypes);
?>
