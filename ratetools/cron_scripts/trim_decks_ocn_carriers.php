<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $starttime = $mtime;
?> 
<?php require_once('/var/www/cdr_analysis/Connections/lerg.php'); ?>
<?php require_once('/var/www/cdr_analysis/Connections/carriers.php'); ?>
<?php

mysql_select_db($database_carriers, $carriers);
$query_get_carriers = "select id, name from carriers where id IN (13,18,19,34,35,36,37,39,40,44) order by name ASC";
$get_carriers = mysql_query($query_get_carriers, $carriers) or die(mysql_error());
$row_get_carriers = mysql_fetch_assoc($get_carriers);
$totalRows_get_carriers = mysql_num_rows($get_carriers);

// Loop through Carriers
do {
	
	$carrier_id = $row_get_carriers['id'];
	
	
	mysql_select_db($database_lerg, $lerg);
	$query_get_prefixes = "select TRIM(TRAILING 'A' FROM CONCAT(LERG.lerg6.npanxx_npa, LPAD(LERG.lerg6.npanxx_nxx, 3, '0'))) as 'npanxx' FROM LERG.lerg6 GROUP BY npanxx";
	$get_prefixes = mysql_query($query_get_prefixes, $lerg) or die(mysql_error());
	$row_get_prefixes = mysql_fetch_assoc($get_prefixes);
	$totalRows_get_prefixes = mysql_num_rows($get_prefixes);
	
	// Loop through prefixes
	do {
		
		// Add a 1 to the front of the prefix since that's how we store them.
		$abloc = '1'.$row_get_prefixes['npanxx'];
		
		// Check to see if the carrier has an ABLOC rate
		mysql_select_db($database_carriers, $carriers);
		$query_get_abloc = "SELECT id, prefix, inter_cost, intra_cost FROM production_rates WHERE carrier_id = '".$carrier_id."' AND prefix = '".$abloc."'";
		$get_abloc = mysql_query($query_get_abloc, $carriers) or die(mysql_error());
		$row_get_abloc = mysql_fetch_assoc($get_abloc);
		$totalRows_get_abloc = mysql_num_rows($get_abloc);
		
		// If we get an ABLOCK check for NPANXXY matches
		if ($row_get_abloc['prefix'] != NULL) {
			
			$abloc_inter_cost = $row_get_abloc['inter_cost'];
			$abloc_intra_cost = $row_get_abloc['intra_cost'];
			
			//echo $carrier_id;
			//echo "----";
			//echo $abloc;
			//echo "----";
			//echo $abloc_inter_cost;
			//echo "----";
			//echo $abloc_intra_cost;
			//echo "\n";
			
			$a0 = $abloc . '0';
			$a1 = $abloc . '1';
			$a2 = $abloc . '2';
			$a3 = $abloc . '3';
			$a4 = $abloc . '4';
			$a5 = $abloc . '5';
			$a6 = $abloc . '6';
			$a7 = $abloc . '7';
			$a8 = $abloc . '8';
			$a9 = $abloc . '9';
						
			mysql_select_db($database_carriers, $carriers);
			$query_get_npanxxy = "SELECT id, prefix, inter_cost, intra_cost FROM production_rates WHERE carrier_id = '".$carrier_id."' AND prefix IN ('".$a0."', '".$a1."', '".$a2."', '".$a3."', '".$a4."', '".$a5."', '".$a6."', '".$a7."', '".$a8."', '".$a9."') ORDER BY prefix ASC";
			$get_npanxxy = mysql_query($query_get_npanxxy, $carriers) or die(mysql_error());
			$row_get_npanxxy = mysql_fetch_assoc($get_npanxxy);
			$totalRows_get_npanxxy = mysql_num_rows($get_npanxxy);
			
			// Loop through our NPANXXY results and delete 7 digit rates that are the same as the 6 digit rate
			if ($totalRows_get_npanxxy != '0') {
				
				do {
				
				$id = $row_get_npanxxy['id'];
				$npanxxy_inter_cost = $row_get_npanxxy['inter_cost'];
				$npanxxy_intra_cost = $row_get_npanxxy['intra_cost'];
				
				if ($abloc_inter_cost === $npanxxy_inter_cost && $abloc_intra_cost === $npanxxy_intra_cost) {
					
					//echo "Delete Rate...";
					//echo $carrier_id;
					//echo "----";
					//echo $row_get_npanxxy['prefix'];
					//echo '<br>';
					mysql_select_db($database_carriers, $carriers);
					$delete_rate = mysql_query("DELETE FROM production_rates WHERE id = '".$id."'", $carriers) or die(mysql_error());
				}
								
				} while ($row_get_npanxxy = mysql_fetch_assoc($get_npanxxy));
				
				
			}
			// End NPANXXY Loop
			
			// A Block Cleanup
			// Next check to see if all of the thousand blocks still exist for a given A block. If so this means we have 0-9 AND an A Block, delete the A block.
			mysql_select_db($database_carriers, $carriers);
			$query_get_thousands = "SELECT id, prefix FROM production_rates WHERE carrier_id = '".$carrier_id."' AND prefix IN ('".$a0."', '".$a1."', '".$a2."', '".$a3."', '".$a4."', '".$a5."', '".$a6."', '".$a7."', '".$a8."', '".$a9."') ORDER BY prefix ASC";
			$get_thousands = mysql_query($query_get_thousands, $carriers) or die(mysql_error());
			$row_get_thousands = mysql_fetch_assoc($get_thousands);
			$totalRows_get_thousands = mysql_num_rows($get_thousands);
			
			if ($totalRows_get_thousands == '10') {
				
				mysql_select_db($database_carriers, $carriers);
				$delete_rate = mysql_query("DELETE FROM production_rates WHERE carrier_id = '".$carrier_id."' AND prefix = '".$abloc."'", $carriers) or die(mysql_error());
				
				
			}
			// End A Block clean up
						
		}
		// End abloc condition	
	
	
	} while ($row_get_prefixes = mysql_fetch_assoc($get_prefixes));
	// End Prefix Loop
	
	
} while ($row_get_carriers = mysql_fetch_assoc($get_carriers));
// End Carrier Loop

mysql_free_result($get_prefixes);

mysql_free_result($get_carriers);
?>
<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $endtime = $mtime;
   $totaltime = ($endtime - $starttime);
   echo "This script ran in ".$totaltime." seconds";
?>
