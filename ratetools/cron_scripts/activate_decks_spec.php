<?php require_once('/var/www/cdr_analysis/Connections/carriers.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
// Get Carriers
mysql_select_db($database_carriers, $carriers);
$query_get_carriers = "SELECT id, name FROM carriers";
$get_carriers = mysql_query($query_get_carriers, $carriers) or die(mysql_error());
$row_get_carriers = mysql_fetch_assoc($get_carriers);
$totalRows_get_carriers = mysql_num_rows($get_carriers);

do {
	// Set Variables
	$carrier_id = $row_get_carriers['id'];

	// Get most recent rate decks for each carrier
	mysql_select_db($database_carriers, $carriers);
	$query_get_ratedecks = "SELECT * FROM ratedecks WHERE effective <= '2018-10-15 00:00:00' AND carrier_id = '".$carrier_id."' ORDER by effective DESC LIMIT 1";
	$get_ratedecks = mysql_query($query_get_ratedecks, $carriers) or die(mysql_error());
	$row_get_ratedecks = mysql_fetch_assoc($get_ratedecks);
	$totalRows_get_ratedecks = mysql_num_rows($get_ratedecks);

	// If most recent deck is not active, deactivate old deck and activate new deck
	//do {
	$ratedeck_id = $row_get_ratedecks['id'];
	$active = $row_get_ratedecks['active'];
	if($active == 0) {
		// Turn Currently Active Deck Off
		$deactivate_ratedeck = mysql_query("UPDATE ratedecks SET active = '0' WHERE carrier_id = '".$carrier_id."' and active = '1'", $carriers) or die(mysql_error());
		
		// Activate New Rate Deck
		$activate_ratedeck = mysql_query("UPDATE ratedecks SET active = '1' WHERE carrier_id = '".$carrier_id."' and id = '".$ratedeck_id."'", $carriers) or die(mysql_error());
	}
	
} while ($row_get_carriers = mysql_fetch_assoc($get_carriers));

?>
<?php
mysql_free_result($get_carriers);

mysql_free_result($get_ratedecks);
?>
