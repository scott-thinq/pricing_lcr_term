<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $starttime = $mtime;
?> 
<?php 
require_once('../../Connections/carriers.php');


mysql_select_db($database_carriers, $carriers);
$query_get_prefixes = "select CONCAT(LERG.lerg6.npanxx_npa, LPAD(LERG.lerg6.npanxx_nxx, 3, '0')) as 'prefix' FROM LERG.lerg6 GROUP BY prefix ORDER BY prefix ASC";
$get_prefixes = mysql_query($query_get_prefixes, $carriers) or die(mysql_error());
$row_get_prefixes = mysql_fetch_assoc($get_prefixes);
$totalRows_get_prefixes = mysql_num_rows($get_prefixes);
	
$lowest_pstn_inter = array();
$lowest_pstn_intra = array();

do {
		
		$lerg_prefix = array();
		// Set variables and start adding digits
		$add9 = '1'. $row_get_prefixes['prefix'] . '9';
		$add8 = '1'. $row_get_prefixes['prefix'] . '8';
		$add7 = '1'. $row_get_prefixes['prefix'] . '7';
		$add6 = '1'. $row_get_prefixes['prefix'] . '6';
		$add5 = '1'. $row_get_prefixes['prefix'] . '5';
		$add4 = '1'. $row_get_prefixes['prefix'] . '4';
		$add3 = '1'. $row_get_prefixes['prefix'] . '3';
		$add2 = '1'. $row_get_prefixes['prefix'] . '2';
		$add1 = '1'. $row_get_prefixes['prefix'] . '1';
		$add0 = '1'. $row_get_prefixes['prefix'] . '0';
		
		array_push($lerg_prefix, $add9, $add8, $add7, $add6, $add5, $add4, $add3, $add2, $add1, $add0);
		
		// Go through each of the 10 possible lerg combos
		foreach($lerg_prefix as $value) {
			
	
			// Start stripping down the 8 digit prefix
			$p8 = $value;
			$p7 = substr($value, 0, 7);
			$p6 = substr($value, 0, 6);
			$p5 = substr($value, 0, 5);
			$p4 = substr($value, 0, 4);
			$p3 = substr($value, 0, 3);
			$p2 = substr($value, 0, 2);
			$p1 = substr($value, 0, 1);
			
		
		// Get the lowest Inter-State rate for the prefix string
		mysql_select_db($database_carriers, $carriers);
		$query_lowest_inter = "SELECT * FROM (SELECT carrier_id, prefix, char_length(prefix) AS diglength, CAST(inter_retail as CHAR) as inter_retail
							   FROM production_rates_pstn
							   WHERE prefix IN ($p8, $p7, $p6, $p5, $p4, $p3, $p2, $p1) 
							   ORDER BY CHAR_LENGTH(prefix) desc) sq1
							   GROUP BY carrier_id ORDER BY inter_retail ASC LIMIT 1";
		$lowest_inter = mysql_query($query_lowest_inter, $carriers) or die(mysql_error());
		$row_lowest_inter = mysql_fetch_assoc($lowest_inter);
		$totalRows_lowest_inter = mysql_num_rows($lowest_inter);
		
		// Do stuff with inter if we get a result
		if ($totalRows_lowest_inter > 0) {
		$prefix = $row_lowest_inter['prefix'];
		$carrier_id = $row_lowest_inter['carrier_id'];
		$inter_retail = $row_lowest_inter['inter_retail'];
	
		// See if we already have that prefix in the deck
		
		if($lowest_pstn_inter) {
		
			if (array_key_exists($prefix, $lowest_pstn_inter))
			{
				$array_chunks = explode(",", $lowest_pstn_inter[$prefix]);
				
				$old_rate = $array_chunks[1];
				
				// If the new retail is lower
				if($inter_retail < $old_rate) {
					
					$lowest_pstn_inter[$prefix] = $carrier_id . "," . $inter_retail;
					//echo "Found Cheaper Rate! Updating...";
					//echo $prefix;
					//echo "---";
					//echo $carrier_id;
					//echo "---";
					//echo $inter_retail;
					//echo "\n";

				}
				
			} else {
				//echo "Adding: ";
				//echo $prefix;
				$lowest_pstn_inter[$prefix] = $carrier_id . "," . $inter_retail;
				//echo "\n";
	
		}
		
		} else {
			//echo "Adding for first time: ";
			//echo $prefix;
			$lowest_pstn_inter[$prefix] = $carrier_id . "," . $inter_retail;
			//echo "\n";
		}
		} // End Do stuff with inter if we get result
		
		// Get the lowest Intra-State rate for the prefix string
		mysql_select_db($database_carriers, $carriers);
		$query_lowest_intra = "SELECT * FROM (SELECT carrier_id, prefix, char_length(prefix) AS diglength, CAST(intra_retail as CHAR) as intra_retail
							   FROM production_rates_pstn
							   WHERE prefix IN ($p8, $p7, $p6, $p5, $p4, $p3, $p2, $p1) 
							   ORDER BY CHAR_LENGTH(prefix) desc) sq1
							   GROUP BY carrier_id ORDER BY intra_retail ASC LIMIT 1";
		$lowest_intra = mysql_query($query_lowest_intra, $carriers) or die(mysql_error());
		$row_lowest_intra = mysql_fetch_assoc($lowest_intra);
		$totalRows_lowest_intra = mysql_num_rows($lowest_intra);
		
		// Do stuff with intra if we get a result
		if($totalRows_lowest_intra > 0) {
		
		$prefix = $row_lowest_intra['prefix'];
		$carrier_id = $row_lowest_intra['carrier_id'];
		$intra_retail = $row_lowest_intra['intra_retail'];
		
		// See if we already have that prefix in the deck
		
		if($lowest_pstn_intra) {
		
			if (array_key_exists($prefix, $lowest_pstn_intra))
			{
				$array_chunks = explode(",", $lowest_pstn_intra[$prefix]);
				
				$old_rate = $array_chunks[1];
				
				// If the new retail is lower
				if($intra_retail < $old_rate) {
					
					$lowest_pstn_intra[$prefix] = $carrier_id . "," . $intra_retail;
					//echo "Found Cheaper Rate! Updating...";
					//echo $prefix;
					//echo "---";
					//echo $carrier_id;
					//echo "---";
					//echo $intra_retail;
					//echo "\n";

				}
				
			} else {
				//echo "Adding: ";
				//echo $prefix;
				$lowest_pstn_intra[$prefix] = $carrier_id . "," . $intra_retail;
				//echo "\n";
	
		}
		
		} else {
			//echo "Adding for first time: ";
			//echo $prefix;
			$lowest_pstn_intra[$prefix] = $carrier_id . "," . $intra_retail;
			//echo "\n";
		}
		} // End do stuff with intra if we get a result
		
		} // End Foreach Loop of this 8 digit prefix
		
		unset($lerg_prefix);

} while ($row_get_prefixes = mysql_fetch_assoc($get_prefixes));

// Loop through Inter-State Array and add to DB
foreach($lowest_pstn_inter as $key => $value) {
	
	// Separate Values
	$value_chunks = explode(",", $value);
	// Set Variables
	$prefix = $key;
	$carrier_id = $value_chunks[0];
	$inter_retail = $value_chunks[1];
	
	// Add to database
	mysql_select_db($database_carriers, $carriers);
	$insert_inter = mysql_query("INSERT INTO PSTN_lowest_inter (prefix, carrier_id, inter_retail) VALUES ($prefix, $carrier_id, $inter_retail)", $carriers) or die(mysql_error());
	
	}
	
// Loop through Intra-State Array and add to DB
foreach($lowest_pstn_intra as $key => $value) {
	
	// Separate Values
	$value_chunks = explode(",", $value);
	// Set Variables
	$prefix = $key;
	$carrier_id = $value_chunks[0];
	$intra_retail = $value_chunks[1];
	
	// Add to database
	mysql_select_db($database_carriers, $carriers);
	$insert_intra = mysql_query("INSERT INTO PSTN_lowest_intra (prefix, carrier_id, intra_retail) VALUES ($prefix, $carrier_id, $intra_retail)", $carriers) or die(mysql_error());
	
	}

unset($lowest_pstn_inter);
unset($lowest_pstn_intra);

   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $endtime = $mtime;
   $totaltime = ($endtime - $starttime);
   echo "This script ran in ".$totaltime." seconds";
?>