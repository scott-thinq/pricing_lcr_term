<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $starttime = $mtime;
?> 
<?php require_once('../../Connections/carriers.php'); ?>
<?php
	mysql_select_db($database_carriers, $carriers);
	$query_get_prefixes = "SELECT prefix FROM production_rates WHERE CHAR_LENGTH(prefix) = 8 GROUP BY prefix";
	$get_prefixes = mysql_query($query_get_prefixes, $carriers) or die(mysql_error());
	$row_get_prefixes = mysql_fetch_assoc($get_prefixes);
	$totalRows_get_prefixes = mysql_num_rows($get_prefixes);
	
	// Loop through 8 digit prefixes
	do {
		
		// Set variables and start chopping off digits
		$p8 = $row_get_prefixes['prefix'];
		$p7 = substr($row_get_prefixes['prefix'], 0, 7);
		$p6 = substr($row_get_prefixes['prefix'], 0, 6);
		$p5 = substr($row_get_prefixes['prefix'], 0, 5);
		$p4 = substr($row_get_prefixes['prefix'], 0, 4);
		$p3 = substr($row_get_prefixes['prefix'], 0, 3);
		$p2 = substr($row_get_prefixes['prefix'], 0, 2);
		$p1 = substr($row_get_prefixes['prefix'], 0, 1);
		
		// Get the lowest Inter-State rate for the prefix string
		mysql_select_db($database_carriers, $carriers);
		$query_lowest_inter = "SELECT * FROM (SELECT carrier_id, prefix, char_length(prefix) AS diglength, CAST(inter_retail as CHAR) as inter_retail
							   FROM production_rates
							   WHERE prefix IN ($p8, $p7, $p6, $p5, $p4, $p3, $p2, $p1) 
							   ORDER BY CHAR_LENGTH(prefix) desc) sq1
							   GROUP BY carrier_id ORDER BY inter_retail ASC LIMIT 1";
		$lowest_inter = mysql_query($query_lowest_inter, $carriers) or die(mysql_error());
		$row_lowest_inter = mysql_fetch_assoc($lowest_inter);
		$totalRows_lowest_inter = mysql_num_rows($lowest_inter);
		
		
		$prefix = $row_lowest_inter['prefix'];
		$carrier_id = $row_lowest_inter['carrier_id'];
		$inter_retail = $row_lowest_inter['inter_retail'];
		
		// See if we already have that prefix in the deck
		mysql_select_db($database_carriers, $carriers);
		$query_existing = "select id, prefix, inter_retail FROM IP_lowest_inter WHERE prefix = $prefix";
		$existing = mysql_query($query_existing, $carriers) or die(mysql_error());
		$row_existing = mysql_fetch_assoc($existing);
		$totalRows_existing = mysql_num_rows($existing);
		
		// If it aint there, insert it
		if($totalRows_existing == 0) {
			
			mysql_select_db($database_carriers, $carriers);
			$insert_inter = mysql_query("INSERT INTO IP_lowest_inter (prefix, carrier_id, inter_retail) VALUES ($prefix, $carrier_id, $inter_retail)", $carriers) or die(mysql_error());
			
		}else{
			
			$id = $row_existing['id'];
			$existing_prefix = $row_existing['prefix'];
			$existing_inter_retail = $row_existing['inter_retail'];
		
			// If the new stuff is cheaper update it
			if($inter_retail < $existing_inter_retail) {
			
				mysql_select_db($database_carriers, $carriers);
				$update_inter = mysql_query("UPDATE IP_lowest_inter SET carrier_id = $carrier_id, inter_retail = $inter_retail WHERE id = $id", $carriers) or die(mysql_error());
			
			}
		
		}
		
		
		mysql_free_result($lowest_inter);
		mysql_free_result($existing);
		
	} while ($row_get_prefixes = mysql_fetch_assoc($get_prefixes));
	
	
mysql_free_result($get_prefixes);
?>
<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $endtime = $mtime;
   $totaltime = ($endtime - $starttime);
   echo "This script ran in ".$totaltime." seconds";
?>