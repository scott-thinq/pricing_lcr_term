<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $starttime = $mtime;
?> 
<?php 
$memcache = new Memcache;
$memcache->connect('127.0.0.1', 11211) or die ("Could not connect");
require_once('../../Connections/carriers.php'); ?>
<?php
	mysql_select_db($database_carriers, $carriers);
	$query_get_prefixes = "SELECT prefix FROM production_rates WHERE CHAR_LENGTH(prefix) = 8 GROUP BY prefix";
	$get_prefixes = mysql_query($query_get_prefixes, $carriers) or die(mysql_error());
	$row_get_prefixes = mysql_fetch_assoc($get_prefixes);
	$totalRows_get_prefixes = mysql_num_rows($get_prefixes);
	
	// Loop through 8 digit prefixes
	do {
		
		// Set variables and start chopping off digits
		$p8 = $row_get_prefixes['prefix'];
		$p7 = substr($row_get_prefixes['prefix'], 0, 7);
		$p6 = substr($row_get_prefixes['prefix'], 0, 6);
		$p5 = substr($row_get_prefixes['prefix'], 0, 5);
		$p4 = substr($row_get_prefixes['prefix'], 0, 4);
		$p3 = substr($row_get_prefixes['prefix'], 0, 3);
		$p2 = substr($row_get_prefixes['prefix'], 0, 2);
		$p1 = substr($row_get_prefixes['prefix'], 0, 1);
		
				
		// Get the lowest Inter-State rate for the prefix string
		mysql_select_db($database_carriers, $carriers);
		$query_lowest_inter = "SELECT * FROM (SELECT carrier_id, prefix, char_length(prefix) AS diglength, CAST(inter_retail as CHAR) as inter_retail
							   FROM production_rates
							   WHERE prefix IN ($p8, $p7, $p6, $p5, $p4, $p3, $p2, $p1) 
							   ORDER BY CHAR_LENGTH(prefix) desc) sq1
							   GROUP BY carrier_id ORDER BY inter_retail ASC LIMIT 1";
		$lowest_inter = mysql_query($query_lowest_inter, $carriers) or die(mysql_error());
		$row_lowest_inter = mysql_fetch_assoc($lowest_inter);
		$totalRows_lowest_inter = mysql_num_rows($lowest_inter);
		
		// Memcache Stuff
		$key = md5($row_lowest_inter['prefix']);
		$get_result = array();
		$get_result = $memcache->get($key);
		
		if(!$get_result) {
			// Insert it in the DB and Add to Cache
		
			//echo "\n";
        	//echo "Carrier ID: " . $row_lowest_inter['carrier_id'] . "\n";
        	//echo "Prefix: " . $row_lowest_inter['prefix'] . "\n";
        	//echo "Inter Retail: " . $row_lowest_inter['inter_retail'] . "\n";
        	//echo "Added to Cache and the Database\n";
        	//echo "\n";
        	$memcache->set($key, $row_lowest_inter, MEMCACHE_COMPRESSED, 20); // Store the result of the query for 20 seconds
					
		}
		
		mysql_free_result($lowest_inter);
		
	} while ($row_get_prefixes = mysql_fetch_assoc($get_prefixes));
	
	
mysql_free_result($get_prefixes);
?>
<?php
   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $endtime = $mtime;
   $totaltime = ($endtime - $starttime);
   echo "This script ran in ".$totaltime." seconds";
?>