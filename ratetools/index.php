<?php require_once('../Connections/carriers.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
// Get Carriers
mysql_select_db($database_carriers, $carriers);
$query_get_carriers = "SELECT * FROM carriers ORDER BY name ASC";
$get_carriers = mysql_query($query_get_carriers, $carriers) or die(mysql_error());
$row_get_carriers = mysql_fetch_assoc($get_carriers);
$totalRows_get_carriers = mysql_num_rows($get_carriers);

// Get Ratedeck Types
mysql_select_db($database_carriers, $carriers);
$query_get_decktypes = "SELECT * FROM ratedeck_types ORDER BY name ASC";
$get_decktypes = mysql_query($query_get_decktypes, $carriers) or die(mysql_error());
$row_get_decktypes = mysql_fetch_assoc($get_decktypes);
$totalRows_get_decktypes = mysql_num_rows($get_decktypes);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="stylesheet.css" rel="stylesheet" type="text/css" />
<link href="rfnet.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascripts/datetimepicker_css.js"></script>
</head>

<body background="#FFFFFF">
<?php
// Set Variables
$list_carriers = $_GET['list_carriers'];
$upload_deck = $_GET['upload_deck'];
?>
<table width="928" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="924" height="493" colspan="8" align="center" valign="top"><table width="608" border="1">
      <tr>
        <td colspan="2">Tools</td>
        </tr>
      <tr>
        <td width="296" class="center_bold">Carrier Actions</td>
        <td width="296" class="center_bold">Rate Actions</td>
      </tr>
      <tr>
        <td><a href="index.php?list_carriers=1">List Carriers</a></td>
        <td><a href="#">List Decks</a></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><a href="index.php?upload_deck=1">Upload Deck</a></td>
      </tr>
    </table>
      <?php if ($list_carriers == 1) { // Show if list carriers selected ?>
  <table width="933" border="1">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td colspan="4"><a href="#">Add Carrier</a></td>
      </tr>
    <tr>
      <td width="104" class="center_bold">Carrier ID</td>
      <td width="150" class="center_bold">Name</td>
      <td width="103" class="center_bold">Type</td>
      <td width="125" class="center_bold">Status</td>
      <td width="142" class="center_bold">Active  Deck</td>
      <td width="137" class="center_bold">Include In Analysis</td>
      <td width="117" class="center_bold">Action</td>
    </tr>
    <?php do { ?>
      <tr>
        <td><center><?php echo $row_get_carriers['id']; ?></center></td>
        <td><?php echo $row_get_carriers['name']; ?></td>
        <td>
        <?php
        // Get Ratedeck Types
		mysql_select_db($database_carriers, $carriers);
		$query_get_type = "SELECT * FROM ratedeck_types where id = '".$row_get_carriers['type']."' ORDER BY name ASC";
		$get_type = mysql_query($query_get_type, $carriers) or die(mysql_error());
		$row_get_type = mysql_fetch_assoc($get_type);
		$totalRows_get_type = mysql_num_rows($get_type);
        
		echo $row_get_type['name'];
		
		mysql_free_result($get_type);
        ?>
        </td>
        <td><?php 
				if($row_get_carriers['active'] == 1) {
					echo "<font color=green>Active</font>";
				}elseif($row_get_carriers['active'] == 0) {
					echo "<font color=red>Inactive</font>";
				}
				//echo $row_get_carriers['active']; ?></td>
        <td>
        <?php
        mysql_select_db($database_carriers, $carriers);
		$query_active_deck = "SELECT id, type, carrier_id, effective, active FROM ratedecks where carrier_id = '".$row_get_carriers['id']."' and active = '1'";
		$active_deck = mysql_query($query_active_deck, $carriers) or die(mysql_error());
		$row_active_deck = mysql_fetch_assoc($active_deck);
		$totalRows_active_deck = mysql_num_rows($active_deck);
        
		if($totalRows_active_deck == 0) {
			echo "<font color=red>No Active Deck</font>";
		} else {
			echo $row_active_deck['effective'];
		}	
		
		?>
        </td>
        <td><center><?php 
				if($row_get_carriers['analysis'] == 1) {
					echo "<font color=green>Yes</font>";
				}elseif($row_get_carriers['analysis'] == 0) {
					echo "<font color=red>No</font>";
				}
				//echo $row_get_carriers['analysis']; ?></center></td>
        <td align="center" valign="middle"><a href="update_carrier.php?carrier_id=<?php echo $row_get_carriers['id']; ?>"><img src ="images/icons/edit_trans.gif" /></a> | <img src ="images/icons/delete_trans.gif" /></td>
      </tr>
      <?php } while ($row_get_carriers = mysql_fetch_assoc($get_carriers)); ?>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="4" align="right">Carriers:&nbsp;<?php echo $totalRows_get_carriers ?></td>
        </tr>
  </table>
  <?php } // Show if list carriers selected ?>
  <?php if ($upload_deck == 1) { // Show if upload deck selected ?>
  <form action="import_ratedeck_2.php" method="post" enctype="multipart/form-data" name="upload_deck" id="upload_deck">
    <table width="500" border="1">
      <tr>
        <td>Carrier:</td>
        <td><select name="carrier_id" id="carrier_id">
          <?php
do {  
?>
          <option value="<?php echo $row_get_carriers['id']?>"><?php echo $row_get_carriers['name']?></option>
          <?php
} while ($row_get_carriers = mysql_fetch_assoc($get_carriers));
  $rows = mysql_num_rows($get_carriers);
  if($rows > 0) {
      mysql_data_seek($get_carriers, 0);
	  $row_get_carriers = mysql_fetch_assoc($get_carriers);
  }
?>
        </select></td>
      </tr>
      <tr>
        <td>Deck Type:</td>
        <td><label for="type"></label>
          <select name="type" id="type">
            <?php
do {  
?>
            <option value="<?php echo $row_get_decktypes['id']?>"><?php echo $row_get_decktypes['name']?></option>
            <?php
} while ($row_get_decktypes = mysql_fetch_assoc($get_decktypes));
  $rows = mysql_num_rows($get_decktypes);
  if($rows > 0) {
      mysql_data_seek($get_decktypes, 0);
	  $row_get_decktypes = mysql_fetch_assoc($get_decktypes);
  }
?>
          </select></td>
      </tr>
      <tr>
        <td>Date/Time Effective:</td>
        <td><input type="Text" id="effective" maxlength="25" size="25" name="effective">
         <a href="javascript:NewCssCal('effective','yyyymmdd','dropdown',true)">
         <img src="images/cal.gif" alt="Pick a date" width="16" height="16" border="0"></a>
        </td>
      </tr>
      <tr>
        <td>Rate Deck:</td>
        <td><input type="file" name="ratedeck" id="ratedeck" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="button" id="button" value="Upload" /></td>
      </tr>
    </table>
  </form>
  <?php } // Show if upload deck selected ?>
  <p>&nbsp;</p></td>
  </tr>
  </table>
</body>
</html>
<?php
mysql_free_result($get_carriers);

mysql_free_result($get_decktypes);
?>
