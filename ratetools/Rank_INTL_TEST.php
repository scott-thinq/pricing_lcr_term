<?php require_once('../Connections/carriers.php'); ?>
<?php require_once('../Connections/lerg.php'); ?>
<?php
/// DROP THE TABLE ///
// Drop Tables First
mysql_select_db($database_carriers, $carriers);
$sql = "DROP TABLE IF EXISTS INTL_Ranking";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
	
// Create table to hold results
mysql_select_db($database_carriers, $carriers);
$sql = " CREATE TABLE `INTL_Ranking` (
  	`id` int(10) NOT NULL AUTO_INCREMENT,
  	`prefix` bigint(20) NOT NULL,
	`rank` int(10) NOT NULL,
  	`carrier_id` int(10) NOT NULL,
	`carrier` varchar(100) NOT NULL,
	`match` bigint(20) NOT NULL,
  	`rate` decimal(14,6) NOT NULL,
  	PRIMARY KEY (`id`,`prefix`),
  	KEY `idx_carrier_prefix` (`carrier_id`,`prefix`),
  	KEY `idx_prefix` (`prefix`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1 ";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Get Unique Prefixes from iLERG
mysql_select_db($database_carriers, $carriers);
//$query_get_prefixes = "select code, char_length(code) as length FROM LERG.iLERG ORDER BY code ASC";
$query_get_prefixes = "select prefix as code, char_length(prefix) as length FROM INTL_TEST GROUP BY code ORDER BY code ASC";
$get_prefixes = mysql_query($query_get_prefixes, $carriers) or die(mysql_error());
$row_get_prefixes = mysql_fetch_assoc($get_prefixes);
$totalRows_get_prefixes = mysql_num_rows($get_prefixes);

do {
	// Clear out the array and reset it for each prefix
	unset($csv_array);
	$csv_array = array();
	
	$code = $row_get_prefixes['code'];
	$prefix = $row_get_prefixes['code'];
	$length = $row_get_prefixes['length'] - 1;
	
	array_push($csv_array,$prefix);

	// Break down the prefix into a csv array decrementing the prefix by one digit for each value
	for ($i=1; $i<=$length; $i++)
		{
		$prefix = substr($prefix,0,-1);
		
		array_push($csv_array,$prefix);
		}
		
	// Make a csv string out of the digit array
	$csv_string = implode(",",$csv_array);
	
	// echo $csv_string . "\n";
	// var_dump($csv_array);
	
	// Lookup the most specific match each carrier has for the prefix
	mysql_select_db($database_carriers, $carriers);
	$query_get_rates = "SELECT * FROM
	(SELECT carrier_id, carriers.`name` as carrier, prefix as 'match', rate
	FROM INTL_TEST
	INNER JOIN carriers on carriers.id = INTL_TEST.carrier_id
	WHERE prefix IN ($csv_string)
	ORDER BY CHAR_LENGTH(prefix) desc) sq1
	GROUP BY carrier_id ORDER BY rate ASC";
	$get_rates = mysql_query($query_get_rates, $carriers) or die(mysql_error());
	$row_get_rates = mysql_fetch_assoc($get_rates);
	$totalRows_get_rates = mysql_num_rows($get_rates);
	
	// echo "Got $totalRows_get_rates results \n";
	
	// Loop through the results and store them.
	$rank = 1;
	do {
		$carrier_id = $row_get_rates['carrier_id'];
		$carrier = $row_get_rates['carrier'];
		$match = $row_get_rates['match'];
		$rate = $row_get_rates['rate'];
		
		// echo "Rank: $rank - " . $code . "-" . $carrier_id . "-" . $carrier . "-" . $match . "-" . $rate . "\n";
		
		// Insert Results
		mysql_select_db($database_carriers, $carriers);
		$insert_results = mysql_query("INSERT INTO INTL_Ranking (prefix, rank, carrier_id, carrier, `match`, rate) VALUES ('".$code."', '".$rank."', '".$carrier_id."', '".$carrier."', '".$match."', '".$rate."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_results);
		
		$rank++;
		
	} while ($row_get_rates = mysql_fetch_assoc($get_rates));
	
} while ($row_get_prefixes = mysql_fetch_assoc($get_prefixes));
?>
