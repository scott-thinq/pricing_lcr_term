<?php require_once('../Connections/carriers.php'); ?>
<?php

////////////////////  GET PREFIXES //////////////////////

// Config Variables//
$carrier_id = 46;
/////////////////////

mysql_select_db($database_carriers, $carriers);
$query_get_prefixes = "SELECT prefix FROM production_rates WHERE carrier_id = $carrier_id ORDER BY prefix ASC";
$get_prefixes = mysql_query($query_get_prefixes, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_prefixes);
$row_get_prefixes = mysql_fetch_assoc($get_prefixes);
$totalRows_get_prefixes = mysql_num_rows($get_prefixes);

// Drop Temporary Tables First
$sql = "DROP TEMPORARY TABLE IF EXISTS COMPARE_Inter";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
$sql = "DROP TEMPORARY TABLE IF EXISTS COMPARE_Intra";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold inter-state results
$sql = " CREATE TABLE `COMPARE_inter` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
		`carrier_id` int(10) NOT NULL,
  		`carrier` varchar(50) NOT NULL,
  		`prefix` bigint(20) NOT NULL,
		`inter` decimal(10,6) NOT NULL,
		`position` int(3) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`)
  		) ENGINE=MYISAM DEFAULT CHARSET=latin1 CHECKSUM=1 ";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// Create temporary table to hold intra-state results
$sql = " CREATE TABLE `COMPARE_intra` (
  		`id` int(10) NOT NULL AUTO_INCREMENT,
		`carrier_id` int(10) NOT NULL,
  		`carrier` varchar(50) NOT NULL,
  		`prefix` bigint(20) NOT NULL,
		`intra` decimal(10,6) NOT NULL,
		`position` int(3) NOT NULL,
  		PRIMARY KEY (`id`,`prefix`)
  		) ENGINE=MYISAM DEFAULT CHARSET=latin1 CHECKSUM=1 ";
mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);

// If we get prefixes do stuff
if ($totalRows_get_prefixes > 0) {
	
	// Loop through prefixes
	do {
		// Set Variables
		$prefix = $row_get_prefixes['prefix'];
		
		// Get Inter-state Cost
		$sql = "SET @position=0;";
		mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
		
		$query_get_inter = "SELECT carrier_id, carriers.name as carrier, prefix, inter_cost as inter, @position:=@position+1 AS position 
		FROM production_rates
		JOIN carriers on carriers.id = production_rates.carrier_id
		WHERE prefix = $prefix
		ORDER BY inter_cost ASC";
		$get_inter = mysql_query($query_get_inter, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_inter);
		$row_get_inter = mysql_fetch_assoc($get_inter);
		$totalRows_get_inter = mysql_num_rows($get_inter);
		
		// Loop through Inter-state results
		do {
			$carrier_id = $row_get_inter['carrier_id'];
			$carrier = $row_get_inter['carrier'];
			$prefix = $row_get_inter['prefix'];
			$inter = $row_get_inter['inter'];
			$position = $row_get_inter['position'];
			
			echo $carrier_id;
			echo "-";
			echo $carrier;
			echo "-";
			echo $prefix;
			echo "-";
			echo $inter;
			echo "-";
			echo $position;
			echo "\n";
			
			// Insert INTO Temp Table
			mysql_select_db($database_carriers, $carriers);
			$insert_inter = mysql_query("INSERT INTO COMPARE_inter (carrier_id, carrier, prefix, inter, position) VALUES ('".$carrier_id."', '".$carrier."', '".$prefix."', '".$inter."', '".$position."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_inter);
			
			} while ($row_get_inter = mysql_fetch_assoc($get_inter));
			
			// Get intra-state Cost
		$sql = "SET @position=0;";
		mysql_query ( $sql ) or die ("Query failed: " . mysql_error() . " Actual query: " . $sql);
		
		$query_get_intra = "SELECT carrier_id, carriers.name as carrier, prefix, intra_cost as intra, @position:=@position+1 AS position 
		FROM production_rates
		JOIN carriers on carriers.id = production_rates.carrier_id
		WHERE prefix = $prefix
		ORDER BY intra_cost ASC";
		$get_intra = mysql_query($query_get_intra, $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $get_intra);
		$row_get_intra = mysql_fetch_assoc($get_intra);
		$totalRows_get_intra = mysql_num_rows($get_intra);
		
		// Loop through intra-state results
		do {
			$carrier_id = $row_get_intra['carrier_id'];
			$carrier = $row_get_intra['carrier'];
			$prefix = $row_get_intra['prefix'];
			$intra = $row_get_intra['intra'];
			$position = $row_get_intra['position'];
			
			echo $carrier_id;
			echo "-";
			echo $carrier;
			echo "-";
			echo $prefix;
			echo "-";
			echo $intra;
			echo "-";
			echo $position;
			echo "\n";
			
			// Insert INTO Temp Table
			mysql_select_db($database_carriers, $carriers);
			$insert_intra = mysql_query("INSERT INTO COMPARE_intra (carrier_id, carrier, prefix, intra, position) VALUES ('".$carrier_id."', '".$carrier."', '".$prefix."', '".$intra."', '".$position."')", $carriers) or die ("Query failed: " . mysql_error() . " Actual query: " . $insert_intra);
			
			} while ($row_get_intra = mysql_fetch_assoc($get_intra));
		
	} while ($row_get_prefixes = mysql_fetch_assoc($get_prefixes));
	
}

?>